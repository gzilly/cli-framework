/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli.spi;

import java.util.Collection;

public interface Command {

    default Collection<Command> commands() {
        return null;
    }

    void execute() throws CommandException;

}
