/*
 * Copyright (C) 2020 Team QUIPSY
 */

package de.quipsy.cli.spi;

public interface Application {

    String programName();

    default String environmentVariable() {
        return this.programName() + "_OPTS";
    }

}
